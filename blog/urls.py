from django.urls import path
from .views import register, profile

urlpatterns = [
    path('login/', register, name='register'),
    path('profile/', profile, name='profile'),
]
